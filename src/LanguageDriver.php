<?php


namespace Lkt\Drivers;

use Lkt\InstancePatterns\Traits\SingleTonTrait;

class LanguageDriver
{
    use SingleTonTrait;

    protected $cookieVariable = 'lang';
    protected $lang = 'en';

    public function __construct(string $cookieVariable = 'lang')
    {
        $this->cookieVariable = trim($cookieVariable);
    }

    public static function setLang($lang = '')
    {
        $instance = static::getInstance();
        $instance->lang = \trim($lang);
        if ($instance->cookieVariable !== '') {
            $_SESSION[$instance->cookieVariable] = $lang;
            $_COOKIE[$instance->cookieVariable] = $lang;
        }
    }

    public static function getLang()
    {
        return \trim(static::getInstance()->lang);
    }
}