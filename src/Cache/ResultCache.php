<?php

namespace Lkt\Drivers\Cache;

use Lkt\InstancePatterns\Interfaces\CacheControllerInterface;
use Lkt\InstancePatterns\Traits\CacheControllerTrait;

class ResultCache implements CacheControllerInterface
{
    use CacheControllerTrait;
}