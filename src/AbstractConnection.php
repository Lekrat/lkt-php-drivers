<?php

namespace Lkt\Drivers;

use Lkt\InstancePatterns\Traits\InstantiableTrait;

abstract class AbstractConnection implements ConnectionInterface
{
    use InstantiableTrait;

    const PORT = 0;

    protected $host = 'localhost';
    protected $user = 'root';
    protected $password = '';
    protected $database = '';
    protected $characterSet = 'UTF-8';
    protected $port = 0;

    protected $connection = null;
    protected $forceRefresh = false;

    public function __construct($connectionInfo = [])
    {
        $connectionInfo = $this->parseConnectionInfo($connectionInfo);
        $this->host = $connectionInfo['Host'];
        $this->user = $connectionInfo['User'];
        $this->password = $connectionInfo['Password'];
        $this->database = $connectionInfo['Database'];
        $this->characterSet = $connectionInfo['CharacterSet'];
        $this->port = $connectionInfo['Port'];
    }

    protected function parseConnectionInfo($connectionInfo = [])
    {
        $connectionInfo['Host'] = isset($connectionInfo['Host']) ? \trim($connectionInfo['Host']) : '';
        $connectionInfo['User'] = isset($connectionInfo['User']) ? \trim($connectionInfo['User']) : '';
        $connectionInfo['Password'] = isset($connectionInfo['Password']) ? \trim($connectionInfo['Password']) : '';
        $connectionInfo['Database'] = isset($connectionInfo['Database']) ? \trim($connectionInfo['Database']) : '';
        $connectionInfo['Port'] = isset($connectionInfo['Port']) ? (int)$connectionInfo['Port'] : static::PORT;
        $connectionInfo['CharacterSet'] = isset($connectionInfo['CharacterSet']) ? \trim($connectionInfo['CharacterSet']) : '';
        $connectionInfo['Headers'] = isset($connectionInfo['Headers']) ? $connectionInfo['CharacterSet'] : [];
        return $connectionInfo;
    }

    public function query($sql = '')
    {
        return [];
    }

    public function forceRefresh($status = true)
    {
        $this->forceRefresh = $status;
        return $this;
    }
}
