<?php


namespace Lkt\Drivers;

use Lkt\Drivers\Cache\QueryCache;
use Lkt\Drivers\Cache\ResultCache;

trait ConnectionCacheTrait
{
    protected static $CACHE_ENABLED = true;

    protected function inCache($sql = '')
    {
        $queryCacheCode = QueryCache::getCacheCodeForQuery($sql);
        return $queryCacheCode !== false;
    }

    protected function loadCache($sql = '')
    {
        $queryCacheCode = QueryCache::getCacheCodeForQuery($sql);
        if ($queryCacheCode !== false) {
            return ResultCache::load($queryCacheCode);
        }
        return [];
    }

    protected function storeCache($sql = '', $results = [])
    {
        $queryCacheCode = QueryCache::getCacheCode();
        QueryCache::store($queryCacheCode, $sql);
        ResultCache::store($queryCacheCode, $results);
        return $this;
    }

    public function clearCache()
    {
        QueryCache::clear();
        ResultCache::clear();
        return $this;
    }

    public function disableCache()
    {
        static::$CACHE_ENABLED = false;
        return $this;
    }

    public function enableCache()
    {
        static::$CACHE_ENABLED = true;
        return $this;
    }

    public function hasCacheEnabled()
    {
        return static::$CACHE_ENABLED;
    }
}