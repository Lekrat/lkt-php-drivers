<?php

namespace Lkt\Drivers;

interface ConnectionInterface
{
    public function query($sql = '');
}